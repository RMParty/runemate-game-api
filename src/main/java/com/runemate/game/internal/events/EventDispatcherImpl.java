package com.runemate.game.internal.events;

import com.google.common.util.concurrent.*;
import com.runemate.client.framework.open.*;
import com.runemate.game.api.hybrid.*;
import com.runemate.game.api.hybrid.entities.*;
import com.runemate.game.api.hybrid.entities.status.*;
import com.runemate.game.api.hybrid.local.hud.interfaces.*;
import com.runemate.game.api.hybrid.location.*;
import com.runemate.game.api.osrs.entities.*;
import com.runemate.game.api.osrs.region.*;
import com.runemate.game.api.script.framework.*;
import com.runemate.game.api.script.framework.core.*;
import com.runemate.game.api.script.framework.listeners.*;
import com.runemate.game.api.script.framework.listeners.dispatchers.*;
import com.runemate.game.api.script.framework.listeners.events.*;
import com.runemate.game.api.script.framework.logger.*;
import java.rmi.*;
import java.util.*;
import java.util.concurrent.*;
import org.jetbrains.annotations.*;

/**
 * Processes and dispatches events to the registered listeners.
 */
public class EventDispatcherImpl extends EventDispatcher {

    private final HashSet<Class<? extends EventListener>> using = new HashSet<>();
    private final ScheduledExecutorService pool = Executors.newScheduledThreadPool(
        3,
        new ThreadFactoryBuilder().setPriority(Thread.MIN_PRIORITY + 1).setDaemon(true).build()
    );
    private final List<EventListener> listeners = new CopyOnWriteArrayList<>();
    private final AbstractBot bot;

    public EventDispatcherImpl(AbstractBot bot) {
        super(bot);
        this.bot = bot;
    }

    @Nullable
    private static Actor getCharacterTarget(int targetIndex) throws RemoteException {
        if (targetIndex == -1) {
            return null;
        }
        if (targetIndex < 0x8000) {
            return OSRSNpcs.getByIndex(targetIndex);
        } else {
            return OSRSPlayers.getAt(targetIndex - 0x8000);
        }
    }

    /**
     * Forwards an event to the registered listeners of the correct type.
     */
    public void dispatchLater(@NotNull Event event) {
        try {
            bot.getPlatform().invokeLater(() -> {
                if (event instanceof HitsplatEvent) {
                    for (final EventListener listener : listeners) {
                        if (listener instanceof HitsplatListener) {
                            ((HitsplatListener) listener).onHitsplatAdded((HitsplatEvent) event);
                        }
                    }
                } else if (event instanceof EngineEvent) {
                    EngineEvent.Type type = ((EngineEvent) event).getType();
                    for (final EventListener listener : listeners) {
                        if (listener instanceof EngineListener) {
                            switch (type) {
                                case CLIENT_CYCLE:
                                    ((EngineListener) listener).onCycleStart();
                                    break;
                                case SERVER_TICK:
                                    ((EngineListener) listener).onTickStart();
                                    break;
                                default:
                                    System.err.println("Unsupported engine event type of " + type);
                            }
                        }
                    }
                } else if (event instanceof CS2ScriptEvent) {
                    final CS2ScriptEvent sEvent = ((CS2ScriptEvent) event);
                    final CS2ScriptEvent.Type type = sEvent.getType();
                    for (final EventListener listener : listeners) {
                        if (listener instanceof CS2ScriptEventListener) {
                            if (CS2ScriptEvent.Type.STARTED.equals(type)) {
                                ((CS2ScriptEventListener) listener).onScriptExecutionStarted(
                                    sEvent);
                            }
                            //Removed for performance reasons
                            /* else if (CS2ScriptEvent.Type.FINISHED.equals(type)) {
                                ((CS2ScriptEventListener) listener).onScriptExecutionFinished(sEvent);
                            } */
                        }
                    }
                } else if (event instanceof EngineStateEvent) {
                    for (final EventListener listener : listeners) {
                        if (listener instanceof EngineListener) {
                            ((EngineListener) listener).onEngineStateChanged(
                                (EngineStateEvent) event);
                        }
                    }
                } else if (event instanceof MenuInteractionEvent) {
                    for (final EventListener listener : listeners) {
                        if (listener instanceof MenuInteractionListener) {
                            ((MenuInteractionListener) listener).onInteraction(
                                (MenuInteractionEvent) event);
                        }
                    }
                } else if (event instanceof AnimationEvent) {
                    for (final EventListener listener : listeners) {
                        if (listener instanceof AnimationListener) {
                            ((AnimationListener) listener).onAnimationChanged(
                                (AnimationEvent) event);
                        }
                    }
                } else if (event instanceof DeathEvent) {
                    for (final EventListener listener : listeners) {
                        if (listener instanceof DeathListener) {
                            ((DeathListener) listener).onDeath((DeathEvent) event);
                        }
                    }
                } else if (event instanceof TargetEvent) {
                    for (final EventListener listener : listeners) {
                        if (listener instanceof TargetListener) {
                            ((TargetListener) listener).onTargetChanged((TargetEvent) event);
                        }
                    }
                } else if (event instanceof PlayerMovementEvent) {
                    for (final EventListener listener : listeners) {
                        if (listener instanceof PlayerMovementListener) {
                            ((PlayerMovementListener) listener).onPlayerMoved(
                                (PlayerMovementEvent) event);
                        }
                    }
                } else if (event instanceof ProjectileLaunchEvent) {
                    for (final EventListener listener : listeners) {
                        if (listener instanceof ProjectileLaunchListener) {
                            ((ProjectileLaunchListener) listener).onProjectileLaunched(
                                (ProjectileLaunchEvent) event);
                        }
                    }
                } else if (event instanceof ItemEvent) {
                    final ItemEvent itemEvent = (ItemEvent) event;
                    for (final EventListener listener : listeners) {
                        SpriteItem item = itemEvent.getItem();
                        if (SpriteItem.Origin.INVENTORY.equals(item.getOrigin())) {
                            if (listener instanceof InventoryListener) {
                                if (itemEvent.getType() == ItemEvent.Type.ADDITION) {
                                    ((InventoryListener) listener).onItemAdded(itemEvent);
                                } else if (itemEvent.getType() == ItemEvent.Type.REMOVAL) {
                                    ((InventoryListener) listener).onItemRemoved(itemEvent);
                                }
                            }
                        } else if (SpriteItem.Origin.EQUIPMENT.equals(item.getOrigin())) {
                            if (listener instanceof EquipmentListener) {
                                if (itemEvent.getType() == ItemEvent.Type.ADDITION) {
                                    ((EquipmentListener) listener).onItemEquipped(itemEvent);
                                } else if (itemEvent.getType() == ItemEvent.Type.REMOVAL) {
                                    ((EquipmentListener) listener).onItemUnequipped(itemEvent);
                                }
                            }
                        }
                    }
                } else if (event instanceof SkillEvent) {
                    final SkillEvent skillEvent = (SkillEvent) event;
                    for (final EventListener listener : listeners) {
                        if (listener instanceof SkillListener) {
                            if (skillEvent.getType() == SkillEvent.Type.LEVEL_GAINED) {
                                ((SkillListener) listener).onLevelUp(skillEvent);
                            } else if (skillEvent.getType() == SkillEvent.Type.EXPERIENCE_GAINED) {
                                ((SkillListener) listener).onExperienceGained(skillEvent);
                            }
                        }
                    }
                } else if (event instanceof MessageEvent) {
                    for (final EventListener listener : listeners) {
                        if (listener instanceof ChatboxListener) {
                            ((ChatboxListener) listener).onMessageReceived((MessageEvent) event);
                        }
                    }
                } else if (event instanceof MoneyPouchEvent) {
                    for (final EventListener listener : listeners) {
                        if (listener instanceof MoneyPouchListener) {
                            ((MoneyPouchListener) listener).onContentsChanged(
                                (MoneyPouchEvent) event);
                        }
                    }
                } else if (event instanceof VarpEvent) {
                    for (final EventListener listener : listeners) {
                        if (listener instanceof VarpListener) {
                            ((VarpListener) listener).onValueChanged((VarpEvent) event);
                        }
                    }
                } else if (event instanceof VarcEvent) {
                    for (final EventListener listener : listeners) {
                        if (listener instanceof VarcListener) {
                            if (((VarcEvent) event).isString()) {
                                ((VarcListener) listener).onStringChanged((VarcEvent) event);
                            } else {
                                ((VarcListener) listener).onIntChanged((VarcEvent) event);
                            }
                        }
                    }
                } else if (event instanceof VarbitEvent) {
                    for (final EventListener listener : listeners) {
                        if (listener instanceof VarbitListener) {
                            ((VarbitListener) listener).onValueChanged((VarbitEvent) event);
                        }
                    }
                } else if (event instanceof LoggedMessageEvent) {
                    for (final EventListener listener : listeners) {
                        if (listener instanceof BotLoggerListener) {
                            ((BotLoggerListener) listener).logged((LoggedMessageEvent) event);
                        }
                    }
                } else if (event instanceof GrandExchangeEvent) {
                    for (final EventListener listener : listeners) {
                        if (listener instanceof GrandExchangeListener) {
                            ((GrandExchangeListener) listener).onSlotUpdated(
                                (GrandExchangeEvent) event);
                        }
                    }
                } else if (event instanceof GroundItemSpawnedEvent) {
                    for (final EventListener listener : listeners) {
                        if (listener instanceof GroundItemListener) {
                            ((GroundItemListener) listener).onGroundItemSpawned(
                                (GroundItemSpawnedEvent) event);
                        }
                    }
                } else if (event instanceof RegionLoadedEvent) {
                    for (final EventListener listener : listeners) {
                        if (listener instanceof RegionListener) {
                            ((RegionListener) listener).onRegionLoaded((RegionLoadedEvent) event);
                        }
                    }
                }
            });
        } catch (final Throwable t) {
            ClientAlarms.handle(bot, t);
            t.printStackTrace();
            if (!bot.isStopped()) {
                bot.stop(t.getMessage());
            }
        }
    }

    /**
     * Registers a listener to dispatch in-game events to
     *
     * @param listener a listener (PaintListener, MouseListener, KeyListener, etc)
     */
    public void addListener(final EventListener listener) {
        listeners.add(listener);
        //The GrandExchangeListener, VarpListener, and VarbitListener are the only listeners that still rely on polling.
        //All other listeners are injection based. We should eventually convert them to native callbacks as well.
        if (listener instanceof VarcListener && !using.contains(VarcListener.class)) {
            VarcDispatcher dispatcher = new VarcDispatcher();
            pool.scheduleWithFixedDelay(dispatcher, 100,
                dispatcher.getIterationRateInMilliseconds(), TimeUnit.MILLISECONDS
            );
            using.add(VarcListener.class);
        }
        if ((listener instanceof VarpListener || listener instanceof VarbitListener) &&
            (!using.contains(VarpListener.class) && !using.contains(VarbitListener.class))) {
            VarbitDispatcher dispatcher = new VarbitDispatcher();
            pool.scheduleWithFixedDelay(dispatcher, 100,
                dispatcher.getIterationRateInMilliseconds(), TimeUnit.MILLISECONDS
            );
            if (listener instanceof VarpListener && listener instanceof VarbitListener) {
                using.add(VarpListener.class);
                using.add(VarbitListener.class);
            } else if (listener instanceof VarpListener) {
                using.add(VarpListener.class);
            } else if (listener instanceof VarbitListener) {
                using.add(VarbitListener.class);
            }
        }
        if (listener instanceof GrandExchangeListener &&
            !using.contains(GrandExchangeListener.class)) {
            GrandExchangeDispatcher dispatcher = new GrandExchangeDispatcher();
            pool.scheduleWithFixedDelay(dispatcher, 100,
                dispatcher.getIterationRateInMilliseconds(), TimeUnit.MILLISECONDS
            );
            using.add(GrandExchangeListener.class);
        }
    }

    @Override
    public void createAnimationEvent(boolean player, long characterUid, int animationId) {
        dispatchLater(new AnimationEvent(
            player ? AnimationEvent.Type.PLAYER : AnimationEvent.Type.NPC,
            () -> player ? new OSRSPlayer(characterUid) : new OSRSNpc(characterUid),
            animationId
        ));
    }

    @Override
    public void createChatboxEvent(int type, @NotNull String sender, @NotNull String message) {
        dispatchLater(new MessageEvent(type, sender, message));
    }

    @Override
    public void createConnectionStateEvent(int old, int current) {
        dispatchLater(new EngineStateEvent(old, current));
    }

    @Override
    public void createDeathEvent(boolean player, long actorUid, int gameCycle) {
        final Actor actor = player ? new OSRSPlayer(actorUid) : new OSRSNpc(actorUid);
        final Coordinate deathPos = actor.getPosition();
        final Area.Rectangular deathArea = actor.getArea();
        dispatchLater(
            new DeathEvent(player ? DeathEvent.Type.PLAYER : DeathEvent.Type.NPC, actor, deathPos,
                deathArea, gameCycle
            ));
    }

    @Override
    public void createEngineCycleEvent() {
        dispatchLater(new EngineEvent(EngineEvent.Type.CLIENT_CYCLE));
    }

    @Override
    public void createGroundItemEvent(int x, int y, int plane, long uid) {
        final Coordinate eventLoc = new Coordinate(x, y, plane);
        final GroundItem item = new OSRSGroundItem(uid, eventLoc);
        dispatchLater(new GroundItemSpawnedEvent(item, eventLoc));
    }

    @Override
    //TODO cleanup parameter types (long endCycle)
    public void createHitsplatEvent(
        boolean player, long actorUid, int typeId, int damage,
        int specialTypeId, int startCyle, int endCycle
    ) {
        final Hitsplat hitsplat = new Hitsplat(typeId, damage, specialTypeId, startCyle, endCycle);
        dispatchLater(
            new HitsplatEvent(
                () -> player ? new OSRSPlayer(actorUid) : new OSRSNpc(actorUid),
                hitsplat
            ));
    }

    @Override
    public void createLoggerEvent(@NotNull BotLogger.Message message) {
        dispatchLater(new LoggedMessageEvent(message));
    }

    @Override
    public void createMenuInteractionEvent(
        int param1, int param2, int opcode, int param0,
        @NotNull String action, @NotNull String target,
        int mouseX, int mouseY
    ) {
        dispatchLater(
            new MenuInteractionEvent(param1, param2, opcode, param0, action, target, mouseX,
                mouseY
            ));
    }

    @Override
    public void createPlayerMovedEvent(long playerUid) {
        dispatchLater(new PlayerMovementEvent(new OSRSPlayer(playerUid)));
    }

    @Override
    public void createProjectileLaunchEvent(long l) {
        dispatchLater(new ProjectileLaunchEvent(new OSRSProjectile(l)));
    }

    @Override
    public void createRegionLoadedEvent(int x, int y) {
        final Coordinate previous = (Coordinate) bot.getCache().get("RegionBase");
        final Coordinate base = new Coordinate(x, y, 0);
        if (Environment.isVerbose()) {
            System.out.println(
                "[Verbose] Changing the cached region base from " + previous + " to " + base);
        }
        bot.getCache().put("RegionBase", base);
        dispatchLater(new RegionLoadedEvent(previous, base));

    }

    @Override
    public void createScriptStartEvent(int scriptId, @Nullable Object[] scriptArgs) {
        dispatchLater(new CS2ScriptEvent(scriptId, CS2ScriptEvent.Type.STARTED, scriptArgs));

    }

    @Override
    public void createServerTickEvent() {
        bot.getEventDispatcher().dispatchLater(new EngineEvent(EngineEvent.Type.SERVER_TICK));
    }

    @Override
    public void createTargetEvent(boolean player, long actorUid, int targetIndex) {
        dispatchLater(new TargetEvent(
            () -> player ? new OSRSPlayer(actorUid) : new OSRSNpc(actorUid),
            () -> getCharacterTarget(targetIndex)
        ));
    }
}
