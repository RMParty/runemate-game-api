package com.runemate.game.api.osrs.region;

import com.runemate.client.game.open.*;
import com.runemate.game.api.hybrid.entities.*;
import com.runemate.game.api.hybrid.queries.results.*;
import com.runemate.game.api.osrs.entities.*;
import java.util.*;
import java.util.function.*;

public final class OSRSPlayers {
    private OSRSPlayers() {
    }


    public static OSRSPlayer getAt(int index) {
        long playerUid = OpenPlayer.getPlayerByIndex(index);
        if (playerUid != -1L) {
            return new OSRSPlayer(playerUid);
        }
        return null;
    }


    public static LocatableEntityQueryResults<Player> getLoaded(
        final Predicate<? super Player> predicate
    ) {
        long[] playerUids = OpenPlayer.getLoadedClean();
        if (playerUids != null) {
            ArrayList<Player> players = new ArrayList<>(playerUids.length);
            for (long playerUid : playerUids) {
                final Player player = new OSRSPlayer(playerUid);
                if (predicate == null || predicate.test(player)) {
                    players.add(player);
                }
            }
            return new LocatableEntityQueryResults<>(players);
        }
        return new LocatableEntityQueryResults<>(Collections.emptyList());
    }


    public static OSRSPlayer getLocal() {
        long uid = OpenPlayer.getLocal();
        return uid != 0 ? new OSRSPlayer(uid) : null;
    }
}
