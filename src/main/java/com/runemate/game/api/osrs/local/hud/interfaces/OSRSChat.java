package com.runemate.game.api.osrs.local.hud.interfaces;

import com.runemate.client.game.open.*;
import com.runemate.game.api.hybrid.local.*;
import com.runemate.game.api.hybrid.local.hud.interfaces.*;
import java.util.*;
import java.util.stream.*;

public final class OSRSChat {
    private final static int CONTAINER = 162;

    private OSRSChat() {
    }

    public static List<Chatbox.Message> getMessages() {
        final ArrayList<Chatbox.Message> messageList = new ArrayList<>(100);
        final List<OpenChatboxMessage> messages = OpenChatboxMessage.getMessages();
        for (OpenChatboxMessage message : messages) {
            messageList.add(new Chatbox.Message(message));
        }
        Collections.sort(messageList);
        return messageList;
    }

    public static List<Chatbox.Message> getMessages(final Chatbox.Message.Type... types) {
        return getMessages(getChatChannels(), types);
    }


    private static List<Chatbox.Message> getMessages(final int channels_uid) {
        return getMessages();
    }

    private static List<Chatbox.Message> getMessages(
        final int channels_uid,
        final Chatbox.Message.Type... types
    ) {
        return getMessages(channels_uid).stream()
            .filter(message -> Arrays.asList(types).contains(message.getType()))
            .collect(Collectors.toList());
    }


    public static int getChatChannels() {
        return (int) OpenChatboxMessage.getChannelsUid();
    }

    public static InterfaceComponent getMessageViewport() {
        return Interfaces.newQuery().containers(CONTAINER).types(InterfaceComponent.Type.CONTAINER)
            .grandchildren(1000).grandchildren(false).results().first();
    }

    public static boolean isChatEffectsOn() {
        return Varps.getAt(171).getValue() == 0;
    }

    public static boolean isPrivateChatSplit() {
        return Varps.getAt(287).getValue() == 1;
    }

    public static boolean isPrivateChatHiddenWhenChatboxIsHidden() {
        return Varps.getAt(1055).getValueOfBit(15) == 1;
    }

}
