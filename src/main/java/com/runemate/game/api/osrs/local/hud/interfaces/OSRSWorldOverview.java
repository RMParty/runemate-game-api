package com.runemate.game.api.osrs.local.hud.interfaces;

import com.runemate.client.game.open.*;
import com.runemate.game.api.hybrid.entities.definitions.*;
import com.runemate.game.api.hybrid.local.*;
import com.runemate.game.api.script.annotations.*;

public class OSRSWorldOverview implements WorldOverview {
    private final long uid;
    private final int id;
    private final int type;
    private final String activity;
    private final String address;

    public OSRSWorldOverview(long uid) {
        this.uid = uid;
        this.id = -1;
        this.type = -1;
        this.activity = null;
        this.address = null;
    }

    public OSRSWorldOverview(int id, int type, String activity, String address) {
        this.address = address;
        this.uid = -1;
        this.id = id;
        this.type = type;
        this.activity = activity;
    }


    @Override
    public int getId() {
        return uid != -1 ? OpenWorld.getNumber(uid) : id;
    }


    @OSRSOnly
    @Override
    public String getActivity() {
        if (uid == -1) {
            return activity;
        }
        return OpenWorld.getActivity(uid);
    }

    private boolean hasSkillTotalRequirement() {
        final int type = getMarkerBits();
        return type != -1 && (type & 0x80) != 0;
    }

    @Override
    public boolean isMembersOnly() {
        final int type = getMarkerBits();
        boolean membersOnly = type != -1 && (type & 0x1) != 0;
        if (getId() == Worlds.getCurrent()) {
            ItemDefinition.p2pWorld = membersOnly;
        }
        return membersOnly;
    }

    @Override
    public boolean isPVP() {
        final int type = getMarkerBits();
        return type != -1 && (type & 0x4) != 0;
    }

    @Override
    public boolean isLootShare() {
        final int type = getMarkerBits();
        return type != -1 && (type & 0x8) != 0;
    }

    @Override
    public boolean isBounty() {
        final int type = getMarkerBits();
        return type != -1 && (type & 0x20) != 0;
    }

    @Override
    public boolean isHighRisk() {
        final int type = getMarkerBits();
        return type != -1 && (type & 0x400) != 0;
    }

    @OSRSOnly
    @Override
    public boolean isLastManStanding() {
        final int type = getMarkerBits();
        return type != -1 && (type & 0x4000) != 0;
    }

    @OSRSOnly
    @Override
    public boolean isSkillTotal500() {
        return hasSkillTotalRequirement() &&
            Integer.parseInt(getActivity().replaceAll("[^0-9]", "")) == 500;
    }

    @OSRSOnly
    @Override
    public boolean isSkillTotal750() {
        return hasSkillTotalRequirement() &&
            Integer.parseInt(getActivity().replaceAll("[^0-9]", "")) == 750;
    }

    @RS3Only
    @Override
    public boolean isQuickChat() {
        return false;
    }

    @RS3Only
    @Deprecated
    @Override
    public boolean isSkillTotal2400() {
        return false;
    }

    @RS3Only
    @Override
    public boolean isSkillTotal2600() {
        return false;
    }

    @RS3Only
    @Override
    public boolean isVIP() {
        return false;
    }

    @RS3Only
    @Override
    public boolean isLegacyOnly() {
        return false;
    }

    @RS3Only
    @Override
    public boolean isEoCOnly() {
        return false;
    }

    @Override
    public boolean isLeague() {
        final int type = getMarkerBits();
        return type != -1 && (type & 0x40000000) != 0;
    }

    @OSRSOnly
    @Override
    public boolean isDeadman() {
        final int type = getMarkerBits();
        return type != -1 && (type & 0x20000000) != 0;
    }

    @OSRSOnly
    @Override
    public boolean isTournament() {
        final int type = getMarkerBits();
        return type != -1 && (type & 0x2000000) != 0;
    }

    @OSRSOnly
    @Override
    public boolean isSkillTotal1250() {
        return hasSkillTotalRequirement() &&
            Integer.parseInt(getActivity().replaceAll("[^0-9]", "")) == 1250;
    }

    @Override
    public boolean isSkillTotal1500() {
        return hasSkillTotalRequirement() &&
            Integer.parseInt(getActivity().replaceAll("[^0-9]", "")) == 1500;
    }

    @OSRSOnly
    @Override
    public boolean isSkillTotal1750() {
        return hasSkillTotalRequirement() &&
            Integer.parseInt(getActivity().replaceAll("[^0-9]", "")) == 1750;
    }

    @Override
    public boolean isSkillTotal2000() {
        return hasSkillTotalRequirement() &&
            Integer.parseInt(getActivity().replaceAll("[^0-9]", "")) == 2000;
    }

    @Override
    public boolean isSkillTotal2200() {
        return hasSkillTotalRequirement() &&
            Integer.parseInt(getActivity().replaceAll("[^0-9]", "")) == 2200;
    }

    public int getMarkerBits() {
        if (uid == -1) {
            return type;
        }
        return OpenWorld.getMarkerBits(uid);
    }

    private int getUnknownType() {
        int mask = getMarkerBits();
        if (isMembersOnly()) {
            mask ^= 0x1;
        }
        if (isPVP()) {
            mask ^= 0x4;
        }
        if (isLootShare()) {
            mask ^= 0x8;
        }
        if (isBounty()) {
            mask ^= 0x20;
        }
        if (hasSkillTotalRequirement()) {
            mask ^= 0x80;
        }
        if (isHighRisk()) {
            mask ^= 0x400;
        }
        if (isLastManStanding()) {
            mask ^= 0x4000;
        }
        if (isTournament()) {
            mask ^= 0x2000000;
        }
        if (isDeadman()) {
            mask ^= 0x20000000;
        }
        if (isLeague()) {
            mask ^= 0x40000000;
        }
        return mask;
    }

    @Override
    public String toString() {
        return "World " + getId() + " Overview";
    }

    public String getAddress() {
        return address;
    }
}
