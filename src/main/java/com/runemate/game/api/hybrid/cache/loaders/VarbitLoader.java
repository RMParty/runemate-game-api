package com.runemate.game.api.hybrid.cache.loaders;

import com.runemate.game.api.hybrid.cache.*;
import com.runemate.game.api.hybrid.cache.elements.*;
import com.runemate.game.cache.item.*;
import java.util.*;

public class VarbitLoader
    extends SerializedFileLoader<com.runemate.game.api.hybrid.cache.elements.CacheVarbit> {
    private final boolean rs3;

    public VarbitLoader(boolean rs3) {
        super(Js5Cache.RS3Archives.JS5_CONFIG);
        this.rs3 = rs3;
    }

    @Override
    protected com.runemate.game.api.hybrid.cache.elements.CacheVarbit construct(
        int entry, int file,
        Map<String, Object> arguments
    ) {
        return new CacheVarbit(rs3);
    }
}
