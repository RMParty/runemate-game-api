package com.runemate.game.api.hybrid.cache;

import java.nio.*;

public final class JByteBuffer {
    private int position;
    private final byte[] buffer;

    public JByteBuffer(final byte[] buffer) {
        this.buffer = buffer;
    }

    public JByteBuffer(final ByteBuffer nio) {
        this.buffer = nio.array();
    }

    public byte[] toByteArray() {
        return buffer;
    }

    public int getRemaining() {
        return getSize() - position;
    }

    public int getSize() {
        return buffer.length;
    }

    public int getPosition() {
        return position;
    }

    public void setPosition(final int index) {
        this.position = index;
    }

    public void readInto(final byte[] buffer) {
        for (int i = 0; i < buffer.length; ++i) {
            buffer[i] = readByte();
        }
    }

    public byte readByte() {
        return buffer[position++];
    }

    /**
     * Reads a 3-byte integer
     */
    public int readTribyte() {
        return (readShort() << 8) + (readByte() & 0xFF);
    }

    public short readShort() {
        return (short) ((readByte() << 8) + (readByte() & 0xFF));
    }

    public int readUnsignedTribyte() {
        return (readUnsignedByte() << 16) + (readUnsignedByte() << 8) + readUnsignedByte();
    }

    public short readUnsignedByte() {
        return (short) (readByte() & 0xFF);
    }

    public int readInteger() {
        return (readShort() << 16) + readShort();
    }

    public float readFloat() {
        return Float.intBitsToFloat(readUnsignedInteger());
    }

    public int readUnsignedInteger() {
        return (readUnsignedShort() << 16) + readUnsignedShort();
    }

    public int readUnsignedShort() {
        return (readUnsignedByte() << 8) + readUnsignedByte();
    }

    public int readBigDynamicInt() {
        if (peakByte() < 0) {
            return readUnsignedInteger() & Integer.MAX_VALUE;
        } else {
            int unsigned = readUnsignedShort();
            return unsigned == 32767 ? -1 : unsigned;
        }
    }

    public int readBigUnsafeDynamicInt() {
        return peakByte() < 0 ? readUnsignedInteger() & Integer.MAX_VALUE : readUnsignedShort();
    }

    public int readShortSmart() {
        return (peakByte() & 255) < 128 ? readUnsignedByte() : readUnsignedShort() - 32768;
    }

    public byte peakByte() {
        return buffer[position];
    }

    public int readQuarterBoundSmart() {
        return (peakByte() & 255) < 128 ? readUnsignedByte() - 64 : readUnsignedShort() - 49152;
    }

    public int readReducedShortSmart() {
        if ((peakByte() & 0xFF) < 128) {
            return this.readUnsignedByte() - 1;
        }
        return this.readUnsignedShort() - 32769;
    }

    public void decipherXTEA(int[] keys, int length_of_encrypted) {
        int initialPosition = position;
        int num_rounds = length_of_encrypted >> 3;
        for (int j1 = 0; j1 < num_rounds; j1++) {
            int k1 = readUnsignedInteger();
            int l1 = readUnsignedInteger();
            int sum = 0xc6ef3720;
            int delta = 0x9e3779b9;
            for (int k2 = 32; k2-- > 0; ) {
                l1 -= keys[(sum & 0x1c84) >>> 11] + sum ^ (k1 >>> 5 ^ k1 << 4) + k1;
                sum -= delta;
                k1 -= (l1 >>> 5 ^ l1 << 4) + l1 ^ keys[sum & 3] + sum;
            }
            this.position = position - 8;
            writeInt(k1);
            writeInt(l1);
        }
        this.position = initialPosition;
    }

    public void writeInt(int i) {
        writeByte((byte) (i >> 24));
        writeByte((byte) (i >> 16));
        writeByte((byte) (i >> 8));
        writeByte((byte) i);
    }

    public void writeByte(final byte b) {
        buffer[position++] = b;
    }
}
