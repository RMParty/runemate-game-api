package com.runemate.game.api.hybrid.cache.configs;

import com.runemate.game.api.hybrid.cache.loaders.*;
import com.runemate.game.internal.exception.*;
import com.runemate.game.cache.*;
import java.io.*;
import java.util.*;
import java.util.function.*;

public class OverlayDefinitions {
    private static final OverlayDefinitionLoader LOADER = new OverlayDefinitionLoader();

    public static OverlayDefinition load(int id) {
        try {
            OverlayDefinition definition =
                LOADER.load(JS5CacheController.getLargestJS5CacheController(), 4, id);
            if (definition != null) {
                return definition;
            }
        } catch (IOException e) {
            throw new UnableToParseBufferException(
                "Failed to load OverlayDefinition with id " + id + "from the Js5Cache.", e);
        }
        return null;
    }

    /**
     * Loads all definitions
     */
    public static List<OverlayDefinition> loadAll() {
        return loadAll(null);
    }

    /**
     * Loads all definitions that are accepted by the filter
     */
    public static List<OverlayDefinition> loadAll(final Predicate<OverlayDefinition> filter) {
        int quantity = LOADER.getFiles(JS5CacheController.getLargestJS5CacheController(), 4).length;
        ArrayList<OverlayDefinition> definitions = new ArrayList<>(quantity);
        for (int id = 0; id <= quantity; ++id) {
            final OverlayDefinition definition = load(id);
            if (definition != null && (filter == null || filter.test(definition))) {
                definitions.add(definition);
            }
        }
        definitions.trimToSize();
        return definitions;
    }
}
