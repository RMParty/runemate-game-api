package com.runemate.game.api.hybrid.location.navigation.web.vertex_types.teleports;

import com.runemate.client.game.account.open.*;
import com.runemate.client.game.open.*;
import com.runemate.game.api.hybrid.entities.*;
import com.runemate.game.api.hybrid.local.*;
import com.runemate.game.api.hybrid.local.hud.interfaces.*;
import com.runemate.game.api.hybrid.location.*;
import com.runemate.game.api.hybrid.location.navigation.web.requirements.*;
import com.runemate.game.api.hybrid.location.navigation.web.vertex_types.*;
import com.runemate.game.api.hybrid.region.*;
import com.runemate.game.api.osrs.local.hud.interfaces.*;
import com.runemate.game.api.rs3.local.hud.*;
import com.runemate.game.api.script.*;
import java.io.*;
import java.util.*;
import lombok.*;

public class TeleportSpellVertex extends TeleportVertex implements SerializableVertex {
    private Spell teleport;

    public TeleportSpellVertex(
        final Spell teleport, final Coordinate destination,
        final Collection<WebRequirement> conditions
    ) {
        super(destination, conditions);
        this.teleport = teleport;
    }

    public TeleportSpellVertex(
        Coordinate position, Collection<WebRequirement> requirements,
        Collection<WebRequirement> forbiddingRequirements, int protocol,
        ObjectInput stream
    ) {
        super(position, requirements, forbiddingRequirements, protocol, stream);
    }

    @Override
    public int hashCode() {
        return teleport.hashCode();
    }

    @Override
    public boolean step() {
        Player local = Players.getLocal();
        if (local == null) {
            return false;
        }
        Coordinate position = local.getPosition();
        if (position == null) {
            return false;
        }
        if (!Bank.isOpen() || Bank.close()) {
            if (teleport.activate()) {
                if (Execution.delayUntil(() -> local.getAnimationId() != -1, 2400, 3600)) {
                    Execution.delayWhile(() -> local.getAnimationId() != -1);
                    if (Execution.delayUntil(() -> !position.equals(local.getPosition()), 2400,
                        3600
                    )) {
                        if (Magic.LUMBRIDGE_HOME_TELEPORT.equals(teleport)
                            || Magic.Lunar.LUNAR_HOME_TELEPORT.equals(teleport)
                            || Magic.Ancient.EDGEVILLE_HOME_TELEPORT.equals(teleport)
                            || Magic.Arceuus.ARCEUUS_HOME_TELEPORT.equals(teleport)) {
                            AccountTraversalProfile atp =
                                OpenAccountDetails.getTraversalProfile();
                            atp.getCachedTimers().put(
                                "home_teleport",
                                new CachedTimer("home_teleport", System.currentTimeMillis())
                            );
                        }
                        return true;
                    }
                }
            }
        }
        return false;
    }

    @Override
    public int getOpcode() {
        return 11;
    }

    @SneakyThrows(IOException.class)
    @Override
    public boolean serialize(ObjectOutput stream) {
        if (teleport instanceof Enum) {
            stream.writeInt(teleport.getSpellBook().getId());
            stream.writeUTF(((Enum) teleport).name());
            return true;
        }
        return false;
    }

    @SneakyThrows(IOException.class)
    @Override
    public boolean deserialize(int protocol, ObjectInput stream) {
        int spellbookId = stream.readInt();
        SpellBook book = Magic.Book.get(spellbookId);
        if (book == null) {
            book = Powers.Magic.Book.get(spellbookId);
        }
        if (book == null) {
            return false;
        }
        String spellName = stream.readUTF();
        if (book == Magic.Book.STANDARD) {
            teleport = Magic.valueOf(spellName);
        } else if (book == Magic.Book.LUNAR) {
            teleport = Magic.Lunar.valueOf(spellName);
        } else if (book == Magic.Book.ANCIENT) {
            teleport = Magic.Ancient.valueOf(spellName);
        } else if (book == Magic.Book.ARCEUUS) {
            teleport = Magic.Arceuus.valueOf(spellName);
        } else if (book == Powers.Magic.Book.STANDARD) {
            teleport = Powers.Magic.valueOf(spellName);
        } else if (book == Powers.Magic.Book.LUNAR) {
            teleport = Powers.Magic.Lunar.valueOf(spellName);
        } else if (book == Powers.Magic.Book.ANCIENT) {
            teleport = Powers.Magic.Ancient.valueOf(spellName);
        } else if (book == Powers.Magic.Book.DUNGEONEERING) {
            teleport = Powers.Magic.Dungeoneering.valueOf(spellName);
        } else {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        Coordinate position = getPosition();
        return "TeleportSpellVertex(spell: " + teleport + ", destination: " + position.getX() +
            ", " + position.getY() + ", " + position.getPlane() + ')';
    }
}
