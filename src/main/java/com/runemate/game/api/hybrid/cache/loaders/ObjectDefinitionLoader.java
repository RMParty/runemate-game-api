package com.runemate.game.api.hybrid.cache.loaders;

import com.runemate.game.api.hybrid.cache.elements.*;
import com.runemate.game.cache.item.*;
import java.util.*;

public class ObjectDefinitionLoader extends
    SerializedFileLoader<com.runemate.game.api.hybrid.cache.elements.CacheObjectDefinition> {
    private final boolean rs3;

    public ObjectDefinitionLoader(boolean rs3, int file) {
        super(file);
        this.rs3 = rs3;
    }

    @Override
    protected com.runemate.game.api.hybrid.cache.elements.CacheObjectDefinition construct(
        int entry,
        int file,
        Map<String, Object> arguments
    ) {
        return new CacheObjectDefinition(rs3, rs3 ? (entry << 8) + file : file);
    }
}
