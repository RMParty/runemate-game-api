package com.runemate.game.api.hybrid.local.hud.interfaces;

import com.runemate.client.framework.open.*;
import com.runemate.game.api.hybrid.entities.*;
import com.runemate.game.api.hybrid.entities.definitions.*;
import com.runemate.game.api.hybrid.entities.details.*;
import com.runemate.game.api.hybrid.input.*;
import com.runemate.game.api.hybrid.local.hud.Menu;
import com.runemate.game.api.hybrid.local.hud.*;
import com.runemate.game.api.hybrid.util.*;
import com.runemate.game.api.osrs.local.hud.interfaces.*;
import com.runemate.game.api.rs3.local.hud.interfaces.*;
import java.awt.*;
import java.util.regex.*;
import javafx.scene.canvas.*;
import javax.annotation.*;

/**
 * An item represented by a Sprite, typically on a InterfaceComponent
 */
public class SpriteItem implements Item, Interactable, Validatable, Renderable {
    private final int id, quantity, index;
    private final Origin origin;

    public SpriteItem(final int id, final int quantity) {
        this(id, quantity, -1, Origin.UNKNOWN);
    }

    public SpriteItem(final int id, final int quantity, final int index, final Origin origin) {
        if (origin == null) {
            throw new IllegalArgumentException(
                "Origin cannot be null, pass Origin.UNKNOWN instead.");
        }
        this.id = id;
        this.quantity = quantity;
        this.index = index;
        this.origin = origin;
    }

    /**
     * Derives a pseudo SpriteItem from this SpriteItem
     */
    public SpriteItem derive(final int quantity) {
        return new SpriteItem(id, this.quantity + quantity, index, origin);
    }

    @Nullable
    public InteractableRectangle getBounds() {
        switch (origin) {
            case BANK:
                return Bank.getBoundsOf(index);
            case BEAST_OF_BURDEN:
                return BeastOfBurden.getBoundsOf(index);
            case EQUIPMENT:
                return Equipment.getBoundsOf(Equipment.Slot.resolve(index));
            case INVENTORY:
                return Inventory.getBoundsOf(index);
            case LOOT_INVENTORY:
                return LootInventory.getBoundsOf(index);
            case SHOP:
                return Shop.getBoundsOf(index);
            case SHOP_FREE:
                return Shop.Free.getBoundsOf(index);
            case OUTGOING_TRADE_OFFER:
                return Trade.Outgoing.getBoundsOf(index);
            case INCOMING_TRADE_OFFER:
                return Trade.Incoming.getBoundsOf(index);
            default:
                return null;
        }
    }

    @Override
    public int getId() {
        return id;
    }

    @Override
    public int getQuantity() {
        return quantity;
    }

    @Override
    @Nullable
    public ItemDefinition getDefinition() {
        return ItemDefinition.get(id);
    }

    public int getIndex() {
        return index;
    }

    public Origin getOrigin() {
        return origin;
    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + quantity;
        result = 31 * result + index;
        result = 31 * result + origin.hashCode();
        return result;
    }

    @Override
    public boolean equals(final Object object) {
        if (object instanceof SpriteItem) {
            final SpriteItem item = (SpriteItem) object;
            return id == item.id && index == item.index && origin == item.origin &&
                quantity == item.quantity;
        }
        return false;
    }

    @Override
    public String toString() {
        if (BotControl.isBotThread()) {
            ItemDefinition def = getDefinition();
            return "SpriteItem(name:" + (def != null ? def.getName() : "N/A (" + id + ")") +
                ", quantity: " + quantity + ", index: " + index + ",  origin: " + origin + ")";
        }
        return "SpriteItem(" + id + " x " + quantity + ", index: " + index + ",  origin: " +
            origin + ")";
    }

    @Override
    public boolean isValid() {
        switch (origin) {
            case BANK:
                return equals(Bank.getItemIn(index));
            case BEAST_OF_BURDEN:
                return equals(BeastOfBurden.getItemIn(index));
            case EQUIPMENT:
                return equals(Equipment.getItemIn(Equipment.Slot.resolve(index)));
            case INVENTORY:
                return equals(Inventory.getItemIn(index));
            case LOOT_INVENTORY:
                return equals(LootInventory.getItemIn(index));
            case SHOP:
                return equals(Shop.getItemIn(index));
            case SHOP_FREE:
                return equals(Shop.Free.getItemIn(index));
            case OUTGOING_TRADE_OFFER:
                return equals(Trade.Outgoing.getItemIn(index));
            case INCOMING_TRADE_OFFER:
                return equals(Trade.Incoming.getItemIn(index));
            case MONEY_POUCH:
                return quantity == MoneyPouch.getContents();
            default:
                return false;
        }
    }

    @Override
    public boolean isVisible() {
        final Rectangle bounds = getBounds();
        if (bounds != null) {
            InteractableRectangle viewport;
            switch (origin) {
                case BANK:
                    return Bank.isOpen() && (
                        (viewport = Bank.getViewport()) != null &&
                            Interfaces.isVisibleInScrollpane(bounds, viewport)
                    );
                case BEAST_OF_BURDEN:
                    return BeastOfBurden.isOpen();
                case EQUIPMENT:
                    return InterfaceWindows.getEquipment().isOpen() &&
                        ((viewport = Equipment.getViewport()) == null || viewport.contains(bounds));
                case INVENTORY:
                    return InterfaceWindows.getInventory().isOpen() &&
                        ((viewport = Inventory.getViewport()) == null || viewport.contains(bounds));
                case LOOT_INVENTORY:
                    return LootInventory.isOpen() &&
                        (
                            (viewport = LootInventory.getViewport()) == null ||
                                Interfaces.isVisibleInScrollpane(bounds, viewport)
                        ) && isValid();
                case SHOP:
                case SHOP_FREE:
                    if (Shop.isOpen()) {
                        InteractableRectangle itemBounds = Shop.getBoundsOf(index);
                        if (itemBounds != null) {
                            viewport = Shop.getViewport();
                            return viewport == null || viewport.contains(itemBounds);
                        }
                    }
                    return false;
                case OUTGOING_TRADE_OFFER:
                case INCOMING_TRADE_OFFER:
                    return Trade.atOfferScreen();
                case LOOTING_BAG:
                    return LootingBag.isOpen();
                case COLLECTION_BOX:
                case PLAYER_APPEARANCE://Should this just always return true...? It may be RS3 only since it's cosmetic...
                case UNKNOWN:
                    break;
            }
        }
        return false;
    }

    @Override
    public double getVisibility() {
        return isVisible() ? 100 : 0;
    }

    @Override
    public boolean hasDynamicBounds() {
        return false;
    }

    @Override
    @Nullable
    public InteractablePoint getInteractionPoint(Point point) {
        final InteractableRectangle bounds = getBounds();
        return bounds != null ? bounds.getInteractionPoint(point) : null;
    }

    @Override
    public boolean contains(final Point point) {
        final Rectangle bounds = getBounds();
        return bounds != null && bounds.contains(point);
    }

    @Override
    public boolean click() {
        return (isVisible() || display()) && Mouse.click(this, Mouse.Button.LEFT);
    }

    @Override
    public boolean hover() {
        return (isVisible() || display()) && Mouse.move(this);
    }

    @Override
    public boolean interact(final Pattern action, final Pattern target) {
        return (isVisible() || display()) && Menu.click(this, action, target);
    }

    @Override
    public void render(Graphics2D g2d) {
        final Rectangle bounds = getBounds();
        if (bounds != null && isVisible()) {
            g2d.drawRect(bounds.x, bounds.y, bounds.width, bounds.height);
        }
    }

    @Override
    public void render(GraphicsContext gc) {
        final Rectangle bounds = getBounds();
        if (bounds != null && isVisible()) {
            gc.strokeRect(bounds.x, bounds.y, bounds.width, bounds.height);
        }
    }

    private boolean display() {
        InteractableRectangle viewport;
        switch (origin) {
            case BANK:
                viewport = Bank.getViewport();
                break;
            case EQUIPMENT:
                InterfaceWindows.getEquipment().open();
                viewport = Equipment.getViewport();
                break;
            case INVENTORY:
                InterfaceWindows.getInventory().open();
                viewport = Inventory.getViewport();
                break;
            case SHOP:
                viewport = Shop.getViewport();
                break;
            case LOOT_INVENTORY:
                viewport = LootInventory.getViewport();
                break;
            default:
                viewport = null;
        }
        return (viewport == null || Interfaces.scrollTo(this, viewport)) && isVisible();
    }


    public enum Origin {
        UNKNOWN,
        BANK,
        BEAST_OF_BURDEN,
        EQUIPMENT,
        INVENTORY,
        GRAND_EXCHANGE,
        LOOT_INVENTORY,
        LOOTING_BAG,
        SHOP,
        SHOP_FREE,
        OUTGOING_TRADE_OFFER,
        INCOMING_TRADE_OFFER,
        COLLECTION_BOX,
        PLAYER_APPEARANCE,
        MONEY_POUCH
    }
}
