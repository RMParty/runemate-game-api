package com.runemate.game.api.script.framework.listeners.events;

import java.util.*;

public class CS2ScriptEvent implements Event {

    private final int id;
    private final Type type;
    private final Object[] arguments;

    public CS2ScriptEvent(int id, Type type, Object[] arguments) {
        this.id = id;
        this.type = type;
        this.arguments = arguments;
    }

    public int getId() {
        return id;
    }

    public Object[] getArguments() {
        return arguments;
    }

    public Type getType() {
        return type;
    }

    @Override
    public String toString() {
        return String.format("CS2ScriptEvent(id=%s, type=%s args=%s)", getId(), getType(),
            Arrays.toString(getArguments())
        );
    }

    public enum Type {
        STARTED//, FINISHED
    }
}
