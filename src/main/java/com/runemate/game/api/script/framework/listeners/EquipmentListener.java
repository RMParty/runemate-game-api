package com.runemate.game.api.script.framework.listeners;

import com.runemate.game.api.script.framework.listeners.events.*;
import java.util.*;

public interface EquipmentListener extends EventListener {
    default void onItemEquipped(final ItemEvent event) {
    }

    default void onItemUnequipped(final ItemEvent event) {
    }
}
