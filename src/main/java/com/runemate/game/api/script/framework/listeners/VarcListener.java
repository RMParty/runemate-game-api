package com.runemate.game.api.script.framework.listeners;

import com.runemate.game.api.script.framework.listeners.events.*;
import java.util.*;

/*
 * TODO: Verify that we receive events from both the legacy and current varc system (they began using an additional map)
 */
public interface VarcListener extends EventListener {
    void onStringChanged(VarcEvent event);

    void onIntChanged(VarcEvent event);
}
