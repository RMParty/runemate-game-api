package com.runemate.game.api.script.framework.listeners;

import com.runemate.game.api.script.framework.listeners.events.*;
import java.util.*;

public interface SkillListener extends EventListener {
    default void onLevelUp(final SkillEvent event) {
    }

    default void onExperienceGained(SkillEvent event) {
    }
}
