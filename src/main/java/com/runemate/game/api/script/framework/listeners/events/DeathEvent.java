package com.runemate.game.api.script.framework.listeners.events;

import com.runemate.game.api.hybrid.entities.details.*;
import com.runemate.game.api.hybrid.location.*;

public class DeathEvent implements Event {
    private final DeathEvent.Type type;
    private final Locatable source;
    private final Coordinate restingPosition;
    private final Area.Rectangular restingArea;
    private final int deathCycle;

    public DeathEvent(
        DeathEvent.Type type, Locatable entity, Coordinate restingPosition,
        Area.Rectangular restingArea, int deathCycle
    ) {
        this.type = type;
        this.source = entity;
        this.restingPosition = restingPosition;
        this.restingArea = restingArea;
        this.deathCycle = deathCycle;
    }

    public int getCycle() {
        return deathCycle;
    }

    public Coordinate getPosition() {
        return restingPosition;
    }

    public Area.Rectangular getArea() {
        return restingArea;
    }

    public Locatable getSource() {
        return source;
    }

    @Override
    public String toString() {
        Locatable source = getSource();
        return String.format(
            "DeathEvent(type=%s, actor=%s, loc=%s, cycle=%s)",
            type.name(), (source instanceof Onymous ? ((Onymous) source).getName() : source),
            restingPosition,
            deathCycle
        );
    }

    public enum Type {
        NPC,
        PLAYER
    }
}
