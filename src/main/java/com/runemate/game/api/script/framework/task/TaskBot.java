package com.runemate.game.api.script.framework.task;

import com.runemate.game.api.script.framework.*;
import java.util.*;

/**
 * An outdated and inferior bot framework.
 *
 * @see com.runemate.game.api.script.framework.tree.TreeBot
 */
@SuppressWarnings("deprecation")
public abstract class TaskBot extends LoopingBot {
    private final Task masterTask = new MasterTask();
    private boolean multibranchExecution;

    @Override
    public final void onLoop() {
        final LinkedList<Task> queue = new LinkedList<>();
        queue.add(masterTask);
        while (!queue.isEmpty()) {
            final Task current = queue.remove();
            if (current.validate()) {
                current.execute();
                if (!multibranchExecution) {
                    queue.clear(); //One task per loop, unless it's a child task
                }
                if (current.hasChildren()) {
                    queue.addAll(current.getChildren());
                }
            }
        }
    }

    /**
     * Adds the tasks to the list to be processed.
     */

    public final void add(final Task... tasks) {
        masterTask.add(tasks);
    }

    /**
     * Removes the tasks from the list.
     */
    public final void remove(final Task... tasks) {
        masterTask.remove(tasks);
    }

    /**
     * Gets a java.util.List of all of the top-level tasks
     *
     * @return a java.util.Task of top-level tasks
     */
    public final List<Task> getTasks() {
        return masterTask.getChildren();
    }

    private static final class MasterTask extends Task {
        @Override
        public boolean validate() {
            return true;
        }

        @Override
        public void execute() {
        }

        @Override
        public void add(Task... children) {
            super.add(children);
            //Make it so the children don't have a parent reference to this object
            for (Task task : children) {
                task.parent = null;
            }
        }
    }
}