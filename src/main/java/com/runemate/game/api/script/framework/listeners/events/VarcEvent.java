package com.runemate.game.api.script.framework.listeners.events;

public class VarcEvent implements Event {
    private final Object previousValue;
    private final Object value;
    private final boolean string;
    private final int index;

    public VarcEvent(boolean string, int index, Object previousValue, Object value) {
        this.string = string;
        this.index = index;
        this.previousValue = previousValue;
        this.value = value;
    }

    public boolean isString() {
        return string;
    }

    public boolean isInt() {
        return !string;
    }

    public Object getPreviousValue() {
        return previousValue;
    }

    public Object getValue() {
        return value;
    }

    @Override
    public String toString() {
        return "VarcEvent{index=" + index + ", " + previousValue + " -> " + value + "}";
    }
}
